export default defineNuxtConfig({
  modules: ["nuxt-primevue"],
  primevue: {
    /* Options */
  },
  css: [
    "primevue/resources/themes/aura-light-green/theme.css",
    "primeflex/primeflex.css",
    "primeicons/primeicons.css",
  ],
  runtimeConfig: {
    public: {
      API_URL: process.env.API_URL,
    },
  },
});
